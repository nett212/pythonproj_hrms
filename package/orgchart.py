import json

class OrgChart:
    def __init__(self, orgJSON, empJSON):
        with open(orgJSON) as json_file:
            orgsdata = json.load(json_file)
        # self.organizations = orgdata
        with open(empJSON) as json_file:
            empsdata = json.load(json_file)
        self.orgList = sorted(orgsdata, key=lambda x: (x['parent_id'],x['organization_name']))
        self.newList = []
        for p in self.orgList:
            if p['is_active'] == True:
                p['org_info'] = f'<Organization(id={p["id"]}): {p["organization_name"]}>'
                self.newList.append(p['org_info'])
        self.organizations = self.Organizations(self.orgList, empsdata)
        self.employees = self.Employees(self.orgList, empsdata)

    class Organizations:
        def __init__(self, sortedOrgsData, empsData):
            self.sortedOrgsData = json.loads(json.dumps(sortedOrgsData))
            self.empsData = empsData
        def list(self):
            orgsList = []
            for org in self.sortedOrgsData:
                orgsList.append(org['org_info'])
            return json.dumps(orgsList, indent=2)
        def get(self, key, val):
            return OrgChart.Organization(self.sortedOrgsData, self.empsData, OrgChart.get(self.sortedOrgsData, key, val))

    class Organization:
        def __init__(self, sortedOrgsData, empsData, singleOrgData):
            self.id = singleOrgData['id']
            self.parent_id = singleOrgData['parent_id']
            self.organization_code = singleOrgData['organization_code']
            self.organization_name = singleOrgData['organization_name']
            self.supervisor_id = singleOrgData['supervisor_id']
            self.is_active = singleOrgData['is_active']
            self.org_info = singleOrgData['org_info']
            self.singleOrgData = singleOrgData
            self.sortedOrgsData = sortedOrgsData
            self.empsData = empsData
            self.supervisor = OrgChart.Employee(sortedOrgsData, empsData, OrgChart.get(empsData, 'employee_no', self.supervisor_id))
            self.employees = self.Employees()
        def Employees(self):
            empInOrg = []
            for employee in self.empsData:
                if self.organization_code == employee['organization_id']:
                    employeeInOrg = OrgChart.Employee(self.sortedOrgsData, self.empsData, employee)
                    empInOrg.append(employeeInOrg.employee_name)
            return empInOrg

    class Employees:
        def __init__(self, sortedOrgsData, empsData):
            self.sortedOrgsData = sortedOrgsData
            self.empsData = empsData
        def get(self, key, val):
            return OrgChart.Employee(self.sortedOrgsData, self.empsData, OrgChart.get(self.empsData, key, val))


    class Employee:
        def __init__(self, sortedOrgsData, empsData, singleEmpData):
            self.sortedOrgsData = sortedOrgsData
            self.empsData = empsData
            self.singleEmpData = singleEmpData
            self.id = singleEmpData['id']
            self.employee_no = singleEmpData['employee_no']
            self.employee_name = singleEmpData['employee_name']
            self.email = singleEmpData['email']
            self.onboarding_date = singleEmpData['onboarding_date']
            self.organization_id = singleEmpData['organization_id']
            self.job_title = singleEmpData['job_title']
            self.gender = singleEmpData['gender']
            self.supervisor = self.Supervisor()
            self.organizations = self.Organizations()
        def Supervisor(self):
            org = OrgChart.get(self.sortedOrgsData, 'organization_code', self.organization_id)
            if org['parent_id'] == 0 and self.employee_no == org['supervisor_id']:
                return None
            supervisor = OrgChart.get(self.empsData, 'employee_no', org['supervisor_id']) 
            return OrgChart.Employee(self.sortedOrgsData, self.empsData, supervisor)
        def Organizations(self):
            orgsList = []
            for org in self.sortedOrgsData:
                if(self.employee_no == org['supervisor_id'] or self.organization_id == org['organization_code']):
                    orgsList.append(org)
            return OrgChart.Organizations(orgsList, self.empsData).list()

                    
    
    def get(searchTypes, key, val):
        itemList = []
        for item in searchTypes:
            if item[key] == val:
                itemList.append(item) 
        if len(itemList) == 1:
            return itemList[0]
        else:
            return itemList