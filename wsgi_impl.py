import json
import wsgiref

from wsgiref.simple_server import make_server

def returnJSON():
    with open('./employees.json') as json_file:
        return json.load(json_file)

x = json.dumps(returnJSON(), indent=2)
def my_app(environ, start_response):
    """a simple wsgi application"""
    status = '200 OK'
    response_headers = [('Content-type', 'application/json')]
    start_response(status, response_headers)
    
    return [x.encode('utf-8')]

httpd = make_server('', 8000, my_app)
print("Serving on port 8000...")
httpd.serve_forever()
